import java.util.ArrayList;
import java.util.List;

public class ListExamples {
    public static void main(String[] args) {
        List<String> wordList = new ArrayList<>();
        wordList.add("ble");
        wordList.add("blee");
        wordList.add("bleee");

        System.out.println(wordList.get(0));
        System.out.println(wordList.get(1));
        System.out.println(wordList.get(2));


        System.out.println("FOREACH");
        for (String word: wordList) {
            System.out.println(word);
        }

        System.out.println("SIZE");
        System.out.println(wordList.size());

        wordList.remove(1);

        System.out.println("CONTAINS");
        System.out.println(wordList.contains("blee"));
        System.out.println(wordList.contains("bleee"));


    }
}
