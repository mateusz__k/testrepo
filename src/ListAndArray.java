import java.util.ArrayList;
import java.util.List;

public class ListAndArray {
    public static void main(String[] args) {
        List<Integer> numberList = new ArrayList<>();

        numberList.add(2);
        numberList.add(3);
        numberList.add(4);
        numberList.add(10);
        numberList.add(21);
        numberList.add(1);
        numberList.add(12);
        numberList.add(1);
        numberList.add(9);
        numberList.add(51);


        int[] arrayOfNumbers = {2, 3, 1, 14, 23, 1, 2, 5, 23, 2};

        System.out.println("Number of duplicates is: " + compareListArray(numberList, arrayOfNumbers));
        newArrayList(1, 2, 6, 2, 1, 5, 4, 3);


//        System.out.println("Your new list: " + printArray(newArrayList(1,2)));

    }


    public static int compareListArray(List<Integer> numberList, int[] arrayOfNumbers) {
        int result = 0;
        for (int i = 0; i < arrayOfNumbers.length && i < numberList.size(); i++) {
            if (arrayOfNumbers[i] == numberList.get(i)) {
                result++;
            }
        }
        return result;
    }

    public static List<Integer> newArrayList(int... numbers) {
        List<Integer> newArrayList = new ArrayList<>();
        for (int number : numbers) {
            newArrayList.add(number);
        }

        return newArrayList;
    }

    public static void printArray(List<Integer> integerList) {
        for (int number : integerList) {
            System.out.println(integerList.get(number));
        }
    }
}

