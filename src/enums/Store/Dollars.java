package enums.Store;

public class Dollars {
    private long dollars;
    private int cents;

    public Dollars(long dollars, int cents) {
        this.dollars = dollars;
        this.cents = cents;
    }

    public long getDollars() {
        return dollars;
    }

    public void setDollars(long dollars) {
        this.dollars = dollars;
    }

    public int getCents() {
        return cents;
    }

    public void setCents(int cents) {
        this.cents = cents;
    }

    public Dollars plus(long dollars, int cents){
        if(cents%100>0) {
            dollars = dollars+1;
            cents = cents+(cents&100);
        }
        Dollars result = new Dollars(getDollars() + dollars, getCents() + cents);
        return result;
    }

    public Dollars plus(Dollars dollars) {
        Dollars result = plus(dollars.dollars, dollars.cents);
        return result;
    }

    public Dollars minus(long dollars, int cents) {
//        if(getCents() - cents<0) {
//            dollars = dollars - 1;                            HMMMMMMMMMMMM jak to zrobic
//        }
        Dollars result = new Dollars(getDollars() - dollars, getCents() - cents);
        return result;
    }

    public Dollars minus(Dollars dollars) {
        Dollars result = minus(dollars.dollars, dollars.cents);
        return result;
    }








    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dollars dollars1 = (Dollars) o;

        if (dollars != dollars1.dollars) return false;
        return cents == dollars1.cents;
    }

    @Override
    public int hashCode() {
        int result = (int) (dollars ^ (dollars >>> 32));
        result = 31 * result + cents;
        return result;
    }

    @Override
    public String toString() {
        return dollars + "," + cents + '$';
    }


    public static void main(String[] args) {
        Dollars d = new Dollars(10,5);
        System.out.println(d.plus(5,99));

        Dollars d2 = d.plus(d);
        System.out.println(d);
    }


}
