package enums.Store;

public class StoreMain {
    public static void main(String[] args) {
        Store store = new Store();
        store.add("maslo",2,Category.FOOD);
        store.add("browar", 4, Category.ALCOHOL);
        store.add("wóda", 10, Category.ALCOHOL);

        System.out.println(store.getAll(Category.ALCOHOL));

    }

}
