package enums.Store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Store {

    private String name;
    private Product product;
    Map<String, Product> products = new HashMap<>();


    public Store(String name, Product product) {
        this.name = name;
        this.product = product;
    }

    public Store() {

    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public Product getProduct() {

        return product;
    }

    public void setProduct(Product product) {

        this.product = product;
    }



    public void add(String name, int price, Category category) {

        Product product = new Product(name, price, category);
        products.put(name, product);


    }

    public void remove(String name) {

        products.remove(name);
    }


    public Product get(String name) {

        return products.get(name);

    }

    public List<Product> getAll(Category category) {
        List<Product> result = new ArrayList<>();
        for (Product product: products.values()) {
            if(product.getCategory().equals(category)) {
                result.add(product);
            }
        }
        return result;
    }


}
