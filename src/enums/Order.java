package enums;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private LocalDate date;
    private Status status;

    public Order(LocalDate date, Status status) {
        this.date = date;
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }



    public static void main(String[] args) {

        List<Order> orders = new ArrayList<>();
        orders.add(new Order(LocalDate.of(2017,7,12), Status.NEW));
        orders.add(new Order(LocalDate.of(2017,6,28), Status.NEW));
        orders.add(new Order(LocalDate.of(2017,8,15), Status.NEW));
        orders.add(new Order(LocalDate.of(2017,8,7), Status.NEW));
        orders.add(new Order(LocalDate.of(2017,8,9), Status.NEW));

        printAll(orders);

        for (Order order: orders) {
            if(order.getDate().isBefore(LocalDate.now().minusDays(7))) {
                order.status = Status.OLD;
            }

            System.out.println(" ");
            System.out.println("UPDATED STATUS");
            System.out.println(" ");

            printAll(orders);
        }
    }

    public static void printAll(List<Order> orders) {
        for (Order order: orders) {
            System.out.println(order.getDate());
            System.out.println(order.getStatus());
        }
    }
}
