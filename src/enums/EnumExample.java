package enums;

public class EnumExample {
    public static void main(String[] args) {
        Priority priority = Priority.HIGH;
        priority = Priority.MEDIUM;

        System.out.println(priority); //domyslna sensowna metoda toString
        Priority[] priorities = Priority.values(); //domyslnie umieszczone w tablicy

        for (Priority priority1: priorities) {
            System.out.println(priority1);          //przyklad foreach
        }

        System.out.println(priority.ordinal()); // domyslnie ponumerowane

        Priority low = Priority.valueOf("LOW");
        System.out.println(low==priority.LOW);

        try {
            Priority medium = Priority.valueOf("m");
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(Priority.LOW.compareTo(Priority.HIGH));






    }
}
