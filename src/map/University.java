package map;

import java.util.HashMap;
import java.util.Map;

public class University {
    private Map<Long, Student> students = new HashMap<>();


    public void addStudent(long indexNumber, String name, String surname) {
        Student student = new Student(indexNumber, name, surname);
        students.put(indexNumber, student);
    }

    public boolean containsStudent(long indexNumber) {
        return students.containsKey(indexNumber);
    }

    public Student getStudent(long indexNumber) {
        Student studentGet = students.get(indexNumber);
        return studentGet;
    }

    public int studentsCount() {
        return students.size();
    }

    public void printAllStudent() {
        for (Student student : students.values()) {
            System.out.println(student.getName() + " " + student.getSurname());

        }
    }


}
