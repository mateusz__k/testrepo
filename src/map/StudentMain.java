package map;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StudentMain {

    public static void main(String[] args) {

        Student bartek = new Student(100200, "Bartosz", "Lewinski");
        Student klepa = new Student(100300, "Karol", "Klepacki");
        Student milena = new Student(100400, "Milena", "Mankowska");

        Map<Integer, Student> studentMap = new HashMap<>();
        studentMap.put(100200, bartek);
        studentMap.put(100300, klepa);
        studentMap.put(100400, milena);

        if(studentMap.containsKey(100200)) {
            System.out.println("There is a student with 100200 index number");
        } else {
            System.out.println("There is no student with 100200 index number");
        }

        Student studentGet = studentMap.get(100400);
        System.out.println(studentGet.getName() + " " +  studentGet.getSurname());
        System.out.println("Number of students: " + studentMap.size());


        System.out.println("All students:");
        for (Student student : studentMap.values()) {
            System.out.println(student.getName() + " " + student.getSurname());

        }








    }
}
