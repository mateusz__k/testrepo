package map;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Person implements Comparable<Person> {
    private String name;
    private Integer age;


    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public int compareTo(Person other) {
        if(this.age == other.age) {
            return 0;
        } else if(this.age > other.age) {
            return -1;
        } else {
            return 1;
        }
    }


    public static void main(String[] args) {

        Queue<Person> peopleQueue = new PriorityQueue<>(new NameComparator());

        peopleQueue.offer(new Person("Jadwiga", 90));
        peopleQueue.offer(new Person("Danuta", 87));
        peopleQueue.offer(new Person("Bartek", 2));
        peopleQueue.offer(new Person("Andrzej", 52));

        while(peopleQueue.size() > 0) {
            Person person = peopleQueue.poll();
            System.out.println(person.getAge() + ", " + person.getName());
        }


        


    }


        static class NameComparator implements Comparator<Person> {

            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        }


}
