package map;

import java.util.HashMap;
import java.util.Map;

public class MapsExample {
    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<>();
        map.put("Jakub", 2);
        map.put("Adam", 4);
        map.put("Weronika", 5);

        System.out.println("Zbior kluczy, wartosci i encje");
        System.out.println(map.keySet());
        System.out.println(map.values());
        System.out.println(map.entrySet());

        System.out.println(map.size());
        System.out.println(map.get("Jakub"));
        System.out.println(map.getOrDefault("Kuba", 1941284)); // JEZELI NIE MA TAKIEGO KLUCZA W TABLICY, ZWRACA INTEGER DEFAULT,
                                                                                // JEZELI NIE OKRESLIMY DEFAULT, ZWRACA NULL

        System.out.println(map.containsKey("Jakub"));
        System.out.println(map.containsKey("Kuba"));        // TYP ZWRACANY BOOLEAN

        map.remove("Adam");
        System.out.println(map.containsKey("Adam"));


    }
}

