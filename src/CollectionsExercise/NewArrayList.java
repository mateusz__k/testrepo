package CollectionsExercise;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;


public class NewArrayList {

    private static List<Integer> newList = new ArrayList<>();
    private static List<Character> newCharList = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number");
        newList.add(scanner.nextInt());
        System.out.println("Enter a number");
        newList.add(scanner.nextInt());
        System.out.println("Enter a number");
        newList.add(scanner.nextInt());
        System.out.println("Enter a number");
        newList.add(scanner.nextInt());
        System.out.println("Enter a number");
        newList.add(scanner.nextInt());


        System.out.println("Your new list:");
        for (Integer entry : newList) {
            System.out.println(entry + " ");
        }

        newCharList.add('a');
        newCharList.add('b');
        newCharList.add('c');
        newCharList.add('d');
        newCharList.add('e');
        newCharList.add('f');
        newCharList.add('e');

        System.out.println("Char counter: " + countChar(newCharList,'e'));

        System.out.println("Integer counter: " + countInt(newList,5));

        String sentence = "ble ble blele lble ble blele ble blelelel";




        System.out.println("Number of words in your sentence is: " + countWords(splitString(sentence)));

    }

    public static int countChar(List<Character> list, char c) {
        int result = 0;
        for (Character ch: newCharList) {
            if(ch==c) {
                result++;
            }
        }
        return result;
    }

    public static int countInt(List<Integer> list, int i) {
        int result = 0;
        for (Integer integer: list) {
            if(integer==i) {
                result++;
            }
        }
        return result;
    }

    public static String[] splitString(String words) {
        String[] parts = words.split(" ");
        return parts;
    }

    public static int countWords(String[] words) {
        int result = 0;
        for (String word: words) {
            result++;
        }
        return result;
    }



}