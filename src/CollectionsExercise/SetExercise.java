package CollectionsExercise;

import java.util.HashSet;
import java.util.Set;

public class SetExercise {
    public static void main(String[] args) {
        HashSet abcSet = new HashSet<Object>(){
            {
                for (char ch = 'a'; ch <= 'z'; ch++)
                    add(ch);
            }
        };
        abcSet.add('ą');
        abcSet.add('ć');
        abcSet.add('ę');
        abcSet.add('ń');
        abcSet.add('ó');
        abcSet.add('ś');
        abcSet.add('ż');
        abcSet.add('ź');
        System.out.println(abcSet);

        if(abcSet.contains('a')) {
            System.out.println("There is 'a' element in abcSet");
        } else {
            System.out.println("No such element in abcSet");
        }

        if(abcSet.contains('ą')) {
            System.out.println("There is 'ą' element in abcSet");
        } else {
            System.out.println("No such element in abcSet");
        }
        if(abcSet.contains('x')) {
            System.out.println("There is 'x' element in abcSet");
        } else {
            System.out.println("No such element in abcSet");
        }
        if(abcSet.contains('z')) {
            System.out.println("There is 'z' element in abcSet");
        } else {
            System.out.println("No such element in abcSet");
        }
        if(abcSet.contains('ź')) {
            System.out.println("There is 'ź' element in abcSet");
        } else {
            System.out.println("No such element in abcSet");
        }
        if(abcSet.contains('ż')) {
            System.out.println("There is 'ż' element in abcSet");
        } else {
            System.out.println("No such element in abcSet");
        }



    }
//     TO NIE DZIALA, DO OGARNIECIA
//    public static void checkSpecialChar (String string) {
//        Set<Character> specialSet = new HashSet();
//        specialSet.add('ą');
//        specialSet.add('ć');
//        specialSet.add('ę');
//        specialSet.add('ń');
//        specialSet.add('ó');
//        specialSet.add('ś');
//        specialSet.add('ż');
//        specialSet.add('ź');
//        Character[] charArray = string.chars().mapToObj(c -> (char) c).toArray(Character[]::new);
//        for (Character ch: charArray) {
//            if(ch.equals(specialSet)) {
//                System.out.println("luz");
//                } else {
//                System.out.println("lipa");
//            }
//        }

    }

    

