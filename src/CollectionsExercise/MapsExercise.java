package CollectionsExercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static jdk.nashorn.internal.runtime.PropertyMap.newMap;

public class MapsExercise {

    public static void main(String[] args) {

        System.out.println("Value of your word is: " + rankWord("bleblexhpąguyfgaywegfiaiufeiab"));


    }

    public static int rankWord(String word) {

        Map<Character, Integer> charMap = new HashMap<Character, Integer>() {
            {

                for (char ch = 'a'; ch <= 'z'; ch++)
                    put(Character.valueOf(ch), 1);

            }
        };


        charMap.put('y', 2);
        charMap.put('h', 2);
        charMap.put('q', 2);
        charMap.put('x', 2);
        charMap.put('ą', 3);
        charMap.put('ę', 3);
        charMap.put('ó', 3);
        charMap.put('ż', 3);
        charMap.put('ź', 3);
        charMap.put('ć', 3);
        charMap.put('ś', 3);



        Character[] charArray = word.chars().mapToObj(c -> (char) c).toArray(Character[]::new);
        int result = 0;
        for (Character character : charMap.keySet()) {
            for (char c : charArray) {
                if(c == character) {
                    result = result + charMap.get(character);
                }
            }
            }
        return result;
    }

}
