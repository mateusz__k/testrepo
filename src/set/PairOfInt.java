package set;

import java.util.HashSet;
import java.util.Set;

public class PairOfInt {
    private int a;
    private int b;

    PairOfInt(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public String toString() {
        return "{"+a+","+b+"}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && (obj.getClass() == PairOfInt.class)) {
            PairOfInt otherPair = (PairOfInt) obj;
            return a == otherPair.a && b == otherPair.b;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = a;
        result = 31 * result + b;
        return result;
    }







    public static void main(String[] args) {

        //błędne użycie zbioru, zbiór traktuje elementy jako unikalne ponieważ mają różne miejsca w pamięci, zbiór
        //myśli że są to unikalne elementy
        Set<PairOfInt> set = new HashSet<>();
        set.add(new PairOfInt(1,2));
        set.add(new PairOfInt(1,2));
        set.add(new PairOfInt(2,1));
        set.add(new PairOfInt(1,1));
        System.out.println(set);




    }

}






