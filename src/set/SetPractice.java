package set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetPractice {
    public static void main(String[] args) {

        Set<Integer> set = new TreeSet<>();

        int[] numbers = {10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55};
        for (int number : numbers) {
            set.add(number);
        }

        System.out.println("size: " + set.size());

        System.out.println("Elementy zbioru: ");
        for (int number: set) {
            System.out.println(number);
        }

        set.remove(10);
        set.remove(12);

        System.out.println("size: " + set.size());

        System.out.println("Elementy zbioru: ");
        for (int number: set) {
            System.out.println(number);
        }

        String ble = "blebleble";
        System.out.println("Tekst zawiera powtorzenia: " + containDuplicates(ble));






    }

    public static boolean containDuplicates(String text){
        char[] chars = text.toCharArray();
        Set<Character> set = new HashSet<>();
        for(char c : chars) {
            set.add(c);
        }
        return set.size() != chars.length;
    }
}

