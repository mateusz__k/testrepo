import java.util.ArrayList;
import java.util.List;

public class ListPractice {
    private static
    List<Integer> integerList = new ArrayList<>();



    public static void main(String[] args) {

        integerList.add(2);
        integerList.add(9);
        integerList.add(1);
        integerList.add(2);


        System.out.println(sumInteger(integerList));

        System.out.println(productInteger(integerList));

        System.out.println(meanInteger(integerList));



    }

    public static int sumInteger(List<Integer> integerList){
        int result = 0;
        for (Integer integer: integerList) {
            result = result+integer;
        }
        return result;
    }


    public static int productInteger(List<Integer> integerList) {
        int result = 1;
        for (Integer integer: integerList) {
            result = result*integer;
        }
        return result;
    }

    public static double meanInteger(List<Integer> integerList) {
        return sumInteger(integerList)/integerList.size();
    }






}
