package InnerClasses.InnerClasses.Village;

public class VillageMain {
    public static void main(String[] args) {
        Village village = new Village(10);
        village.getFarmer().work();
        System.out.println(village.getFood());
    }
}
