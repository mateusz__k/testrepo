package InnerClasses.InnerClasses.Village;

public class Village {
    int food;
    Farmer farmer = new Farmer();

    public Village(int food) {
        this.food = food;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public int getFood() {
        return food;
    }

    public class Farmer {



        public void work() {
            food = food + 10;
        }
    }
}
