package InnerClasses.InnerClasses.CarExample;

public class Car {
    private boolean open = false;
    private Door door = new Door();

    public Door getDoor() {
        return door;
    }

    public void setDoor(Door door) {
        this.door = door;
    }

    public boolean isOpen() {
        return open;
    }





    public class Door {
        public void open() {
            open = true;
        }

        public void close() {
            open = false;
        }

    }
}
