package InnerClasses.InnerClasses.Family;


public class Family {

    private Mother mother = new Mother("Irena");;
    private Father father = new Father("Andrzej");


    public Family() {
    }


    public Mother getMother() {
        return mother;
    }

    public Father getFather() {
        return father;
    }

    public void introduce() {
        mother.introduce();
        father.introduce();
    }


    public class Mother {
        String name;

        public Mother(String name) {
            this.name = name;
        }

        public void introduce() {
            System.out.println("Nazywam się " + name + ", mój mąż to " + father.name);
        }

    }

    public class Father {
        String name;

        public Father(String name) {
            this.name = name;
        }

        public void introduce() {
            System.out.println("Nazywam się " + name + ", moja żona to " + mother.name);
        }
    }

}
