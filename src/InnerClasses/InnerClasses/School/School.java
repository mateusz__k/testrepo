package InnerClasses.InnerClasses.School;

public class School {
    private String name;
    private String patron;


    public Pupil newPupil(String name) {
        Pupil pupil = new Pupil(name);
        return pupil;
    }


    public School(String name, String patron) {
        this.name = name;
        this.patron = patron;
    }

    public String getName() {
        return name;
    }


    public String getPatron() {
        return patron;
    }


    public class Pupil {
        private String name;

        public Pupil(String name) {
            this.name = name;
        }


        public void introduce(Pupil pupil) {
            System.out.println("Nazywam się " + name + ". Uczęszczam do szkoły " + School.this.getName() + " imienia " + School.this.getPatron() + ".");
        }
     }

}
