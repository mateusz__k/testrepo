package InnerClasses.InnerClasses.School;

public class SchoolMain {
    public static void main(String[] args) {
        School school = new School("LO nr 2", "Andrzej Kowalski");
        School.Pupil pupil = school.newPupil("Tomek");
        pupil.introduce(pupil);
    }
}
