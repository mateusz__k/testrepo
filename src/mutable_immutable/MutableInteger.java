package mutable_immutable;

public class MutableInteger {
    private int value;

    public MutableInteger(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MutableInteger that = (MutableInteger) o;

        return value == that.value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override

    public String toString() {
             return String.valueOf(value);
    }
}
