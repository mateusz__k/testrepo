package mutable_immutable;

public class StringImmutable {
    public static void main(String[] args) {
        String imie = "jan";
        imie.toUpperCase(); //nie dziala bo nie mozna edytowac

        System.out.println(imie);

        imie = imie.toUpperCase(); //dziala, podmieniamy na nowe imie (tak nalezy robic, bo string jest immutable)

        System.out.println(imie);
    }
}
