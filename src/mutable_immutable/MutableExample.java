package mutable_immutable;

public class MutableExample {
    public static void main(String[] args) {


        MutableInteger a = new MutableInteger(100);
        MutableInteger b = new MutableInteger(200);

        System.out.println(a.equals(b));
        a.setValue(3);
        System.out.println(b);

    }
}
