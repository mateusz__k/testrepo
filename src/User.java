import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private String password;

    User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static void main(String[] args) {

        List<User> userList = new ArrayList<>();

        User andrzej = new User("Andrzej","KORKI");
        User dobro = new User("Dobromir", "TigerBlack");
        User paulina = new User("Paulina", "123123");
        User SDA = new User("Admin", "JavaJava");

        userList.add(andrzej);
        userList.add(dobro);
        userList.add(paulina);
        userList.add(SDA);


        printUser(userList);





    }

    public static void printUser(List<User> userList){
        for (User user: userList)
              {
                  System.out.println(user.getName());
              }
    }

}
